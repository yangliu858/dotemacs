\section{Widom's Pseudo Differential Calculus}

We start with a smooth manifold $M$ with a connection $\nabla$. Denote by $C^\infty(\mathcal T^r_s M$ the space of smooth complex valued tensor field on  $M$ which are $r$-times contravariant and $s$-times covariant. Given any $u\in C^\infty(\mathcal T^r_s M$, in a local coordinate system $(x,\xi)$, $\nabla u$ is a $(r,s+1)$-tensor, follow the classical notations:
\begin{align}
    u_{;p} \defeq \nabla_p u = \partial_{x_k} u^{j_1 \dots j_r}_{k_1 \dots k_s} + \sum_{l = 1}^m \Gamma^i_{pk_l}  
    \label{eq:wpse_nabla}
\end{align}

Let $\op{Sym}$ denote the symmetrization of tensors in call their indices, and put the symmetric tensor $\stensor$:
\begin{align}
    u \stensor v \defeq \op{Sym}( u \tensor v)
    \label{eq:wpse_sym}
\end{align}
then
\begin{align}
    \op{Sym}(\nabla^k u \tensor v) = \sum_{j=0}^k \binom ki (\op{Sym}(\nabla^j u))\stensor 
    \op{Sym}(\nabla^{k-j} v).
    \label{eq:wpse_sym-nabla}
\end{align}

Finally, we will denote the symmetrized covariant derivative by $\mathcal D$, that is

\begin{align}
    \mathcal D^k u  = \op{Sym}(\nabla^k u).  
    \label{eq:wpse_sym_derivative}
\end{align}
We have the higher order product rule:
\begin{align}
    \mathcal D^k (u_1 \cdots u_r) =\sum\frac{k!}{i_1 \cdots i_r}\mathcal D^{i_1} u_1 \stensor \dots \stensor \mathcal D^{i_r} u_r,
    \label{eq:wpse_sym_derivative_prodrule}
\end{align}
where the sum is over $i_1 + \cdots i_r = k$.

Given on $\R^n$ with symbol $p(x,\xi) \in C^\infty(\R^n,\R^n)$, the  pseudo differential operator $P$ 
is given by 
\begin{align}
    (P f)(x) = \int_{\R^n}e^{-i \xi\cdot (x -y} p(x,\xi)f(y)dyd\eta
\end{align}
The function $l(x_0,\xi,y) = \xi \cdot (y-x)$ plays a significant role in the quantization map above. The generalization to manifolds is a smooth function $\mathfrak l(\xi_x,y) \in C^\infty(T^*M\times M)$. The linearity in $\xi$ becomes the linearity of $\mathfrak l$ on each fiber of $T^*M$, but, the linearity in $x$ has no straightforward analogy. However, when the manifold $M$ is equipped with a connection $\nabla$ (on the cotangent bundle of $M$), the linearity at $x_0$ can be
described as the vanishing of the symmetrized covariant derivative (along the $y$ variable) $\mathcal D^k \mathfrak l(\xi_x,y)$ at $x = y$ for any $k\ge 2$.  


\begin{defn}\label{defn:wpse_defn-linearfun}
    Let $M$ be a smooth manifold with a connection $\nabla$ (on the cotangent) bundle and let $\mathcal D^k$ 
    be the $k$-symmetrized covariant derivative with respect to $\nabla$ as before. A linearization of the manifold $M$ is a real-valued smooth function $\mathfrak l(\xi_x,y)\in C^\infty(T^*M\times M)$ such that for fixed base point $x_0$, $\mathfrak l$ is linear in $\xi \in T^*_{x_0}M$ and such that for all $\xi_x$, the covariant derivative (along $y$):
    \begin{align}
        \mathcal D^k \mathfrak l(\xi_x,y) = 
        \begin{cases}
            v, & k=1, \\ 0, & k\neq 1.
        \end{cases}
        \label{eq:wpse_defn-linearfun}
    \end{align}

\end{defn}
The existence of such functions was proved in, \cite[Proposition 2.1]{MR560744}. A typical example of $\mathfrak l$ near by a point $x$ is $\mathfrak l(\xi_x,y) = \abrac{\xi_x, \exp^{-1}_x y}$, where $\exp$ is the exponential map defined by the connection $\nabla$.

\begin{defn}
    \label{defn:wpse-symbol-map}
Let $M$ be a smooth manifold. For any pseudo differential operator $P:C^\infty(M)\rightarrow C^\infty(M)$, the symbol $\sigma(P)$ of $P$ is a smooth function on $T^*M$:
\begin{align}
    \sigma(P)(\xi_x)  = P e^{i \mathfrak l(\xi_x,y)} \Big|_{y=x}.
    \label{eq:wpse_symmap}
\end{align}
\end{defn}

Equipped with linearization function $\mathfrak l$, we can derive a Taylor's expansion formula for smooth functions on $M$. If we fixed a point $y\in M$, consider $\mathfrak l (\xi_x,y)$, which is a linear functional on the fiber $T^*_x M$. Therefore, $\mathfrak l(\cdot,y)$ can be thought as vector field on $M$ (supported near by the point $y$). With the interpretation, we denote:
\begin{align}
    \mathfrak l(\cdot,y)^k = \mathfrak l(\cdot,y)\tensor \cdots \tensor \mathfrak l(\cdot,y),
    \label{eq:wpse_tensor_l}
\end{align}
which is a symmetric $k$-th order contravariant tensor field and so the pairing 
\begin{align}
    \nabla^k f(x_0) \cdot \mathfrak l(x_0,x)^k
\end{align}
is well-defined and, by the symmetry of $l(x_0,x)^k$, 
\begin{align}
   \nabla^k f(x_0)\cdot \mathfrak l(x_0,x)^k = \mathcal D^k f(x_0) \mathfrak l(x_0,x)^k,
\end{align}
where $\symd^k$ is the symmetrization of $\nabla^k$ as before. Now we are ready to state the Taylor's expansion formula.

\begin{prop}
    \label{prop:wpse_taylor_exp_on_M}
    For each point $x_0 \in M$ and each integer $N$ the function 
    \begin{align}
        f(x) - \sum_{j=0}^N \frac{1}{j!}  \nabla^j f(x_0) \cdot \mathfrak l(x_0,x)^j 
    \end{align}
    vanished to order $N+1$ at $x_0$. 
\end{prop}

We introduce operators on $C^\infty(T^*M)$ that play the roles of $\partial_\xi$ and $\partial_x$ do in Euclidean space. First, we have to deal with certain tensor fields on $T^*M$. More precisely, we pull back the bundle $\mathcal T^r_s M$ to $T^*M$, we will still called the sections tensor fields of rank $(r,s)$ on $T^*M$, denoted by $C^\infty(\mathcal B^r_s T^*M$. From an analytic point of view, the only difference between $C^\infty(\mathcal T^r_s T^* M)$ and $C^\infty(\mathcal
B^r_s T^*M)$ is that the coordinates of the latter fields depend on $(x,\xi)$.\par

For a function $p(x,\xi) \in C^\infty(T^*M)$, we define $D^k p$ to be the $k$-th derivative of $p$ in the direction of the fibers of $T^*M$. We think of $p(x,\cdot)$ as a function on $T^*_xM$ when the base point $x$ is fixed, then the $k$-th derivative $D^k f$ gives rise to a $k$-linear functional on $T^*_x M$ and so can be identified with an element of $\tensor_k T_{x_0}M$. Hence $D^k p$ is a contravariant $k$-tensor. This is the analogue of $\partial_\xi^k$. Indeed, the analogy can be
demonstrate by an example. Given  $f \in
C^\infty(M)$, $\nabla^k f$ is a covariant $k$-tensor, thus the pairing $\nabla^k f \cdot D^k p$ produces a scalar.In a local coordinate $(x_1, \dots x_n)$, then $\xi_x = \sum_{j=0}^n \xi_j dx_j|_x$, we denote $\mathfrak l_j(x,y) = \mathfrak l(dx_j|_x, y)$, then for any indices $j_1, \dots, j_k$:
\begin{align}
    k! \partial_{\xi_{j_1}} \cdots \partial_{\xi_{j_k}}p(\xi_x) = 
    \symd^k_y \mathfrak l_{j_1}(x,y) \cdots \mathfrak l_{j_k}(x,y) \cdot D^k p(\xi_y)\Big|_{y=x}.
    \label{eq:wpse-hnabla-incoord}
\end{align}

The analogue of $\partial_x$ is not that obvious. Indeed, we define a covariant derivative, still denoted by $\nabla$, $\nabla: C^\infty(T^*M) \rightarrow C^\infty(\mathcal B^1_0 M)$:
\begin{align}
    \nabla^j p(\xi_x) \defeq \nabla^j p(d_y \mathfrak l(\xi_x,y))\Big|_{y = x},\,\,\, j \in N.
    \label{eq:wpse_hnabla}
\end{align}
Although, the function $\mathfrak l$ is not unique, all derivatives at $y = x$ are the same. Thus $\nabla^j p$ in  \eqref{eq:wpse_hnabla} is a well-defined covariant $j$-tensor. Observe that 
\begin{align}
    d_y\mathfrak l(\xi_x,y) \Big|_{y=x} = \xi_x,
\end{align}
moreover, we can see that  $\nabla$ and $D$ commute, thus the mixed derivative
\begin{align}
    \nabla^k D^j p(\xi_x)   \defeq \nabla^k_y D^j p(d_y\mathfrak l(\xi_x,y))\Big|_{y=x}.
    \label{eq:wpse_mixed_derivative}
\end{align}
gives rise to a tensor of type $(j,k)$. The subscript $y$ in $\nabla_y$ and $d_y$ indicates the variable with respect to which the derivatives are taken. 


\marginnote{\tiny Todo: two lemmas}

Now we are ready to state the asymptotic product formula of two symbols.
\begin{prop}
    \label{eq:wpse-asym-sybproduct}
    Let $M$ be a compact manifold equipped with a connection $\nabla$ on the cotangent bundle, as before, the covariant derivative $\mathcal D$ is the symmetrization of $\nabla$. Given two pseudo differential operators $P$ and $Q$ acting on $C^\infty(M)$ with symbols $p(\xi_x)$ and $q(\xi_x)$ as functions on the cotangent bundle. Then the symbol of the operator $PQ$, upto a smoothing operator, has the asymptotic expansion:
    \begin{align}
        & p \star q (\xi_x) 
        \backsim  \sum \frac{i^{k - \abs m - \abs p - p_0}}{p_0!k! m! p!} \nonumber \\ & \mathcal D^{p_1}
        \mathcal D^{m_1}\mathfrak l(\xi_x,x) \stensor \cdots \stensor \mathcal D^{p_k} \mathcal D^{m_k} \mathfrak l(\xi_x,x) \stensor \mathcal D^{p_0} D^{\abs m} q(\xi_x) D^{\abs p + p_0} p(\xi_x),
        \label{eq:wpse-asym-sybproduct}
    \end{align}
    where $m = (m_1, \dots,m_k)$ and $p = (p_1, \dots, p_k)$ are multi-indices and the factorials have the usual definition, for example,  $m! = m_1! \cdots m_k!$. The summation is over
    \begin{align}
        \abs p +p_0 = k, \,\,\, m_1, \dots, m_k \ge 2. 
    \end{align}
\end{prop}
\marginnote{\tiny Todo:use multi-index notations to simplify the expansion}


The summation on the right hand side can be viewed as a sequence of bi-differential operators $\set{a_j(\cdot,\cdot)}_{j=0}^\infty$ acting on $p$ and $q$, if one collects the terms in such a way that 
\begin{align}
    a_j(\cdot,\cdot): S\Sigma^{d_1} \times S\Sigma^{d_2} \rightarrow S\Sigma^{d_1 + d_2 - j},
\end{align}
then the first a few $a_j$ are 
\begin{align}
    a_0(p,q) & = pq, \label{eq:wpse-a0}\\
    a_1(p,q) & =  D p \mathcal D q, \label{eq:wpse-a1}\\
    a_2(p,q) & = \frac 12 D^2 p \mathcal D^2 q + \frac 12 \mathcal D \mathcal D^2 \mathfrak l D p \mathcal D^2 q.
    \label{eq:wpse-a2}
\end{align}

\marginnote{\tiny Todo:computation of $\symd^j \symd^k \mathfrak l$}













