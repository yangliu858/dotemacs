\section{Deformation Along $\T^{n}$}
\label{sec:deformation-along-tn}
\subsection{Deformation of Fr\'echet Algebras} \label{subsec:deform-frech-algebr}
In this paper, we mainly work on complete locally convex topological vector
spaces with a $n$-torus action. 
\begin{defn}\label{defn:deformation-along-tn}
Let $V$ be a complete locally convex topological vector
space whose topology is defined by an increasing
family of semi-norms $\norm{\cdot}_{k}$. We say $V$ is a  smooth
$\T^{n} = \R^{n}/\Z^{n}$ module if $V$ admits a periodic $\R^{n}$
action 
\begin{align*}
 \alpha_{t}: V \rightarrow V, \,\,\,\, t\in\R^{n}, 
\end{align*}
such that $\alpha_{t}(v)$ belongs to $C^{\infty}(\R^{n},V)$ for all $v
\in V$. 
\end{defn}

Recall that the $C^{\infty}$-topology on
$C^{\infty}(\R^{n},V)$ is given the semi-norms 
\begin{align}
  \label{eq:smooth-topology-seminorms}
  \norm{F}_{j,k} \defeq \sup_{i\leq j} \nsum{\abs\mu\leq k}{} \frac{1}{\mu!} \sup_{u\in \R^{n}} \norm{\partial^{(\mu)}F(u)}_{i},
\end{align}
 where $\mu = (\mu_{1},\cdots, \mu_{n})$ is a multi-index. 

%Then for any $(j,k)\in
%  \N\times\N$, there exists an integer $l> 0$ such that 
%  \begin{align}
%    \label{eq:smooth-action-condition}
%    \norm{\alpha_{t}(v)}_{j,k} \leq C_{j,k} \norm{v}_{l}, \,\,\,
%    \forall v \in V. 
%  \end{align}

Due to the duality between $\Z^{n}$ and $\T^{n}$, a $\T^{n}$ action on
$V$ is equivalent to a $\Z^{n}$ grading structure. Indeed, given an
periodic $\R^{n}$ action on a complete locally convex topology space
$V$, Fourier theory gives us that  
\begin{align*}
  V = \overline{ \dsum{r\in\Z^{n}}{} V_{r}},
\end{align*} 
where $V_{r}$ is the image of the projection $p_{r}: V \rightarrow V$:
\begin{align}\label{eq:peter-weyl-decom}
  p_{r}(v) = \int_{\T^{n}} \alpha_{t}(v)e^{-2\pi i r \cdot x} dt,
  \;\;\; v\in V. 
\end{align}


\begin{prop}\label{prop:deform-frech-algebr-smoothness}
Let $V$ be a smooth $\T^{n}$-module as in definition
\ref{defn:deformation-along-tn}, whose topology is given by an
countable increasing family of semi-norms $\norm{\cdot}_{j}$ with
$j\in\N$. Then for any element $v\in V$ and a fixed index $j$, the sequence $\norm{p_{r}(v)}_{j}$
has rapidly decay in $r \in \Z^{n}$, where $p_{r}$ is the projection
defined in \eqref{eq:peter-weyl-decom}. More precisely, for any integer $k,j>0$, there exist a polynomial
$Q_{k}(x_{1},\cdots,x_{n})$ of degree $k$ and an integer $l$ such that $\forall v \in V$, 
\begin{align}
  \label{eq:peter-weyl-decom-estimate}
  \norm{p_{r}(v)}_{j} \leq C \frac{C_{k,l}}{\abs{Q_{k}(r)}}
 \norm{\alpha_{t}(v)}_{j,k} , 
\end{align}
where $\norm{\cdot}_{j,k}$ is defined in \eqref{eq:smooth-topology-seminorms}.
Therefore the infinite sum 
\begin{align*}
  \nsum{r\in\Z^{n}}{} v_{r}, \;\; v_{r} = p_{r}(v)
\end{align*}
converges absolutely to $v$ with respect to all semi-norms $\norm{\cdot}_{j}$.
\end{prop}

\begin{proof}
Let $\Delta$ be the Laplacian operator on $\T^{n}$. For $r \in
\R^{n}$, let $Q(r)$ be the polynomial such that $\Delta( e^{2 \pi i
  r\cdot t}) = Q(r) e^{2 \pi i r\cdot t}$. Let $k>0$ be an integer, apply integration by parts
$k$ times  on the integral in \eqref{eq:peter-weyl-decom}, we get: for all $v \in V$ and
$r \in\Z^{n}$:
\begin{align*}
  p_{r}(v) = \int_{\T^{n}} e^{2\pi i t \cdot r}\alpha_{t}(v)dt
  =\int_{\T^{n}} \frac{1}{Q^{k}(r)}e^{2\pi i t \cdot r}
  \Delta^{k}\brac{\alpha_{t}(v)}dt.
\end{align*}
Hence for each $j$
\begin{align*}
  \norm{p_{r}(v)}_{j} \leq \frac{C}{\abs{Q^{k}(r)}} \norm{\alpha_{t}(v)}_{j,2k},
\end{align*}
where $\norm{\cdot}_{j,2k}$ is defined in \eqref{eq:smooth-topology-seminorms}.
\end{proof}






Conversely, suppose $V$ admits a smooth $\Z^{n}$ grading: $V = \overline{ \dsum{r\in\Z^{n}}{} V_{r}}$, then the
$\T^{n}$ action is given by on each homogeneous component $V_{r}$
% the $\T^{n}$ module structure is defined as follows, the torus action
% on homogeneous element $v_{r} \in
% V_{r}$ is given by:
\begin{align}
  \label{eq:z-grading-torus-action}
  t\cdot v_{r} = e^{2\pi i t\cdot  r} v_{r}, \,\,\, t\in\T^{n}. 
\end{align}
A vector $v =\nsum{r\in
    \Z^{n}}{} v_{r} \in V$ is smooth respect to the torus action if and only
if for each semi-norm $\norm{\cdot}_{j}$, the sequence $\norm{v_{r}}_{j}$ decays faster than any polynomial in $r$, that is
  for each semi-norm $\norm{\cdot}_{j}$ and integer $k$, there is an integer $l$ and a
  constant $C_{j,k}$, such that 
  \begin{align}\label{eq:peter-weyl-decay}
    \norm{v_{r}}_{j} \leq C_{j,k} \frac{\norm{v}_{l}}{r^{k}}.
  \end{align} 

% \begin{prop}
%   Any $v \in V$ can be written as a infinite sum $v = \nsum{r\in
%     \Z^{n}}{} v_{r}$, where $v_{r} = p_{r}(v)$, moreover
  
% \end{prop}

% \begin{proof}
% Notice that   $\alpha_{x}(v)$ is a smooth function in $x$, apply
% integration by parts $k$-times on \eqref{eq:peter-weyl-decom}, with
% the estimate \eqref{eq:smooth-action-condition}, we can conclude~\eqref{eq:peter-weyl-decay}

% \end{proof}








Now let us assume that $A$ is a smooth $\Z^{n}$-graded algebra
whose topology is given by an family of increasing semi-norms
$\norm{\cdot}_{j}$. Of course, we require that the multiplication in
$A$ preserve the grading, that is for any $a_{r} \in A_{r}$, $b_{s}\in
A_{s}$, the product $a_{r}b_{s} \in A_{r+s}$. Also the multiplication
is assumed to be jointly continuous, that is
for every $j$, there is a $k$ and a constant $C_{j}$ such that 
\begin{align}
  \label{eq:jointly-continuity}
\norm{ab}_{j}\leq C_{j} \norm{a}_{k}\norm{b}_{k}, \,\,\, \forall a,b \in A.
\end{align}


\begin{defn}\label{defn:defomred-alg}
Fix a skew symmetric $n\times n$ matrix $\Theta$, we obtain a family
of deformed algebra $A_{\Theta}$ whose underlying topological vector
space is $A$ endowed with a deformed multiplication $\times_{\Theta}$
given by:
\begin{align}
  \label{eq:deformed-product}
  a \times_{\Theta} b = \nsum{r,s\in\Z^{n}}{} e^{\pi i r \cdot(\Theta
    s)}a_{r}b_{s}, \,\,\, \forall a,b \in A,
\end{align}
and $a = \nsum{r}{}a_{r}$, $b = \nsum{s}{}b_{s}$ are the Peter-Weyl
decomposition as above.  
  \end{defn}

\begin{prop}\label{prop:deformation-along-tn-associativity-theta-multi}
  The deformed product $\times_{\Theta}$ on $A_{\Theta}$ is
  associative:
  \begin{align}
    \label{eq:associativity-theta-multi}
    \brac{a \times_{\Theta} b} \times_{\Theta} c = a\times_{\Theta}\brac{b \times_{\Theta} c}
  \end{align}
\end{prop}
\begin{proof}
Let $a,b,c\in A_{\Theta}$ with the decompositions: $a =
\nsum{r}{}a_{r}$, $b = \nsum{s}{}b_{s}$ and $c = \nsum{l}{}c_{l}$, we
compute
\begin{align*}
 \brac{a \times_{\Theta} b} \times_{\Theta} c & = \nsum{k,l}{}e^{\pi i k\cdot(\Theta l)}
 \brac{\nsum{r+s = k}{} e^{\pi i r\cdot(\Theta s)} a_{r}b_{s}} c_{l}
 \\
&= \nsum{r,s,l}{} e^{\pi i r\cdot(\Theta s)} e^{\pi i r\cdot(\Theta
  l)} e^{\pi i s\cdot(\Theta l)} a_{r}b_{s}c_{l},
\end{align*}
here we have used the estimate \eqref{eq:peter-weyl-decay} implicitly
so that we do not need to care about the order of summation in the all
infinite sums above with multi-indices. Similar computations shows
that  
\begin{align*}
  a\times_{\Theta}\brac{b \times_{\Theta} c}  = \nsum{r,s,l}{} e^{\pi i r\cdot(\Theta s)} e^{\pi i r\cdot(\Theta
  l)} e^{\pi i s\cdot(\Theta l)} a_{r}b_{s}c_{l}
\end{align*}
as well. 
\end{proof}



\begin{prop}
  The new product is still jointly continuous: for each $j$, there is
  a $k$ and a constant $C_{\Theta,j}$ such that 
  \begin{align}
    \label{eq:jointly-continuity-deformed-product}
 \norm{ a \times_{\Theta} b}_{j} \leq C_{\Theta,j} \norm{a}_{k} \norm{b}_{k}   
  \end{align}
\end{prop}
\begin{prop}
The action $\alpha$ on $A_{\Theta}$ is  a differentiable action and by
algebra automorphisms with respect to the $\times_{\Theta}$ multiplication.  
\end{prop}





\begin{prop}\label{prop:functorial-property} 
  Let $\phi: A \rightarrow B$ be a $\T^{n}$-equivariant algebra
  homomorphism, where $A$, $B$ are two algebra which admits a
  deformation as above. If we identify $A$ and $A_{\Theta}$, $B$ and
  $B_{\Theta}$ by the identity maps respectively, then 
  \begin{align}
    \label{eq:functorial-deformed-product}
    \phi: A_{\Theta}\rightarrow B_{\Theta}
  \end{align}
is still an $\T^{n}$-equivariant algebra homomorphism with respect to
the new product $\times_{\Theta}$. 
\end{prop}

\begin{prop}\label{sec:general-set-up-*-operator}
Let the algebra $A$ be a smooth $\T^{2}$-module as above. Suppose $A$ has an continuous involution $*$ and $\T^{2}$ action on
$A$ are all $*$-automorphisms. For a fixed skew symmetric matrix
$\Theta$, the deformed algebra $A_{\Theta}$ is still a $*$-algebra
with the same $*$ operator, that is
\begin{align}
  \label{eq:*-operator}
  (a\times_{\Theta} b)^{*} = b^{*}\times_{\Theta}a^{*}.
\end{align} 
\end{prop}






\begin{prop}\label{prop:general-set-up-prop-trace}
Let $\tau: A_{\Theta} \rightarrow \C$ be a trace, that is $\tau$ is a
continuous linear functional on $A_{\Theta}$ and satisfies the trace
property $\tau(a \times_{\Theta} b) = \tau(b \times_{\Theta} a)$. If
we identify $A_{\Theta}$ with $A$ as a topological vector space via
the identity map, still denote by $\tau: A\rightarrow\C$ the
associated linear functional. Then $\tau$ is a trace
on $A$, that is $ \tau(ab) = \tau (ba)$ with respect to the
undeformed product. 
  
\end{prop}
\begin{proof}
Let $a_{r}, b_{l}\in A_{\Theta}$ be homogeneous elements of degree
$r,l$ respectively, moreover we assume $r +s \neq 0$, then 
\begin{align*}
  \tau(a_{r}\times_{\Theta} b_{l}) = \tau(b_{l}\times_{\Theta} a_{r})
  =  \tau (e^{2 \pi i \abrac{l,\Theta r}} a_{r}\times_{\Theta} b_{l}),
\end{align*}
hence
\begin{align*}
  (1 - e^{2 \pi i \abrac{l,\Theta r}})\tau(a_{r}\times_{\Theta} b_{l})
  = 0,
\end{align*}
since $r+l\neq 0$, $1 - e^{2 \pi i \abrac{l,\Theta r}}\neq 0$, we
conclude that  
\begin{align}
  \label{eq:trace-1}
 \tau(a_{r}\times_{\Theta} b_{l}) = 0, \,\,\,\, \text{for $r+l \neq 0$.} 
\end{align}
Now let $a,b\in A$ with Peter-Weyl decomposition:$a = \nsum{}{}a_{r}$
and $b = \nsum{}{}b_{l}$. 
\begin{align*}
  \tau(a b) &= \nsum{r,l}{} \tau(a_{r}b_{l}) =  e^{\pi i\abrac{l,\Theta
    r}}\tau (a_{r}\times_{\Theta} b_{l}) = \\
&= \nsum{r+l = 0}{} \tau(a_{r} \times_{\Theta} b_{l}) = 
\nsum{r+l = 0}{} \tau(b_{l} \times_{\Theta} a_{r}) \\
&= \nsum{r+l = 0}{} \tau(b_{l} a_{r}) = \tau(ba).
\end{align*}

\end{proof}



% Now let assume $A$ has a $\T^{n}$-equivariant representation: $\pi: A
% \rightarrow B(H)$, where $B(H)$ is the algebra of bounded operator on
% the Hilbert space $H$,  ``$\T^{n}$-equivariant'' means the following: 
% \begin{enumerate}
% \item The torus $\T^{n}$ acts on $H$ unitarily: for each $t\in
%   \T^{n}$, $t$ acts on $H$ as a unitary operator, denoted by $U_{t}$;
% \item For $t\in\T^{n}$, $\pi(t\cdot a) = \op{Ad}_{t}(\pi(a)) = U_{t}\pi(a)U_{-t}$.
% \end{enumerate}
% Then we can deformed $\pi$ into   $\pi^{\Theta}: A_{\Theta}
% \rightarrow B(H)$ as follows: first decompose $H$ into the direct sum
% of eigenspaces of $\T^{n}$. For $a = \nsum{r}{}a_{r} \in A_{\Theta}$
% and $f = \nsum{r}{}f_{r} \in H$, define
% \begin{align}
%   \label{eq:theta-reprentation}
%   \pi^{\Theta}(a)(f) = \nsum{r,l\in \Z^{n}}{} e^{\pi i\abrac{r,\Theta l}}\pi(a_{r})(f_{l}).
% \end{align}

This deformation construction is ``functorial'' in the following
sense. Given three $\T^{n}$-smooth modules $\mathcal V_{1}$, $\mathcal
V_{2}$ and $\mathcal V_{3}$ and a bilinear map $m: \mathcal
V_{1}\times \mathcal V_{2} \rightarrow \mathcal V_{3}$ which satisfies
a ``jointly continuity'' estimate as in
\eqref{eq:jointly-continuity}. Fix a  skew symmetric $n\times n$
matrix $\Theta$,  $m$ can be deformed into
$m_{\Theta}: \mathcal
V_{1}\times \mathcal V_{2} \rightarrow \mathcal V_{3}$ by defining 
\begin{align*}
  m_{\Theta}(v_{1},v_{2}) = \nsum{r,l\in\Z^{n}}{}e^{\pi i \abrac{r,\Theta l}} (v_{1})_{r} (v_{2})_{l}.
\end{align*}
For instance, let $A$ be a $\T^{n}$-smooth algebra and $E$ is a
$\T^{n}$-smooth $A$-bimodule, then the left (or right) action on $A$
on $E$ admits a deformation so that  is a $E$ is
$A_{\Theta}$-bimodule. 

The deformation requirements can be relaxed when dealing with
filtrated algebras. Let $A$ be a filtrated algebra with a filtration: 
\begin{align}
  \label{eq:deformation-of-algebra-filtration}
  \cdots\subset A_{-j}\subset\cdots \subset A_{0}\subset \cdots A_{j}\cdots\subset A.
\end{align}
Suppsoe  each $A_{j}$ ($j\in\Z$) is a Fr\'echet space whose topology
is defined by a increasing countably many semi-norms
$\set{\norm{\cdot}_{l,j}}_{l\in\N}$. The multiplication preserves the filtration:
\begin{align}
  \label{eq:deformation-of-algebra-multiplication}
  m: A_{j_{1}} \times A_{j_{2}} \rightarrow A_{j_{1} + j_{2}}
\end{align}
such that the continuity condition holds: for fixed $j_{1}$ and
$j_{2}$ as above and for each positive integer $l$, one can find a
integer $k$ and constant $C_{k,j_{1},j_{2}}$ such that  
\begin{align}
  \label{eq:deformation-of-algebra-multiplication-continuity-condition}
 \norm{m(a_{1}a_{2})}_{l,j_{1} + j_{2}} \leq C_{k,j_{1},j_{2}} \norm{a_{1}}_{k,j_{1}}
 \norm{a_{2}}_{k,j_{2}}, \,\,\,\, \forall a_{1}\in A_{j_{1}}, \,\, a_{2}\in A_{j_{2}}.
\end{align}

Assume each $A_{j}$ is a smooth $\T^{n}$-module with respect to the
semi-norms $\set{\norm{\cdot}_{l,j}}$ above, for a fixed skew
symmetric matrix $\Theta$, 
\begin{align}
  \label{eq:eq:deformation-of-algebra-m-theta}
  m_{\Theta}: A_{j_{1}} \times A_{j_{2}} & \rightarrow A_{j_{1} + j_{2}} \\
           (a_{1},a_{2}) &\mapsto \nsum{r,l\in\Z^{n}}{}e^{\pi i \abrac{r,\Theta l}} m\brac{ (a_{1})_{r} (a_{2})_{l}}
\end{align}
defines a new multiplication on $A$, again, the new algebra will be
denoted by $A_{\Theta}$. 

%\subsection{Deformation of Operators} 
\input{deformation_ops}


\subsection{Non-Commutative Toric Manifolds and Soblev Spaces $\mathcal H_{s}$}
Let $M$ be a compact Riemmanian manifold. Assume the groups of
isometries $\op{Iso(M)}$ contains a $n$-torus $\T^{n}$, denoted the
torus action by 
\begin{align*}
 \alpha: \T^{n}\rightarrow \op{Iso}(M): t \rightarrow \alpha_{t},
\end{align*}
The space of smooth functions $C^{\infty}(M)$ becomes a
$\T^{n}$-module by the action:
\begin{align}\label{eq:torus-action-on-functions}
  U_{t}:C^{\infty}(M)\rightarrow C^{\infty}(M): U_{t}(f)(x) = f(\alpha_{-t}(x)).
\end{align}
Let us equip $C^{\infty}(M)$ with the semi-norms of partial
derivatives, it is easy to check that for each $f \in C^{\infty}(M)$, $U_{t}(f)$ is a smooth
$C^{\infty}(M)$-valued functions in $t\in\T^{n}$ and the estimate
\eqref{eq:smooth-action-condition} holds. Therefore for any $n\times
n$ skew symmetric matrix $\Theta$, the new multiplication
$\times_{\Theta}$  is well-defined as in \eqref{eq:deformed-product}.  
\begin{defn}
For a fixed $n\times n$ skew symmetric matrix $\Theta$, the smooth $\T^{2}$-algebra
$C^{\infty}(M_{\theta})$ admits a deformation $C^{\infty}(M_{\Theta})$
who underlying topological vector space is   $C^{\infty}(M)$ endowed
with a deformed product $\times_{\Theta}$ defined in
\eqref{eq:deformed-product}. The usual $*$ operator on $C^{\infty}(M)$
makes $C^{\infty}(M_{\Theta})$ into a $*$-algebra by proposition 
\ref{sec:general-set-up-*-operator}. 
\end{defn}

Let $\int$ be the integration against the volume
form (with respect to the flat metric) on $M$ which is still a
trace on $C^{\infty}(\T^{2}_{\theta})$ by proposition
\ref{prop:general-set-up-prop-trace}, we define a pairing
$\phi:C^{\infty}(M_{\Theta}) \times
C^{\infty}(M_{\Theta})\rightarrow \C$: 
\begin{align*}
  \phi(f,g) = \int (g^{*}\times_{\Theta}f).
\end{align*}
Observe that the completion of the pre-Hilbert space
$(C^{\infty}(M_{\Theta}), \phi)$ is exactly the space of
$L^{2}$-functions on $M$, denote it by $\mathcal H$. Then the left
$\times_{\Theta}$-multiplication $f \mapsto L^{\Theta}_{f}$ makes
$C^{\infty}(M_{\Theta})$ a subalgebra of $B(\mathcal H)$, then
operator norm completion give rise a $C^{*}$-algebra $C(M_{\Theta})$
which is the non-commutative analogy of continuous functions on the
non-commutative manifold $M_{\Theta}$. Observe that the operator norm
is close related to the matrix $\Theta$. Therefore, in general, elements in 
$C(M_{\Theta})$ can not be written as the deformation
$\pi^{\Theta}(L_{f})$ for some $f\in C(M)$. However the
$C^{*}$-version $C(M_{\Theta})$ is not very closed to the differential
geometry on $M_{\Theta}$ we are interested in this paper.  

%\subsection{$\T^{n}$-equivariant bundle and Soblev Spaces $\mathcal H_{s}$}
\begin{defn}\label{defn:deform-pseudo-diff-1-equivariant-bundle}
  Let $M$ be a compact Riemannian manifold with a $n$-torus action,
  denoted by $\alpha$ as
  isometries as before. Let $E \rightarrow M$ be a smooth Hermitian
  bundle equipped with a $\T^{n}$ action as well, denoted by $\tilde \alpha$, then the smooth section $\Gamma(E)$ is a $\T^{n}$-module via:
  \begin{align}
    \label{eq:torus-action-on-bundles}
    \tilde \alpha_{t}(\psi)(x) =
    \tilde\alpha_{t}(\psi(\alpha^{-1}_{t}(x))), \,\,\, t\in\T^{n},
    \psi\in \Gamma(E). 
  \end{align}
The Hermitian bundle $E$ is called $\T^{n}$-equivariant if the
$C^{\infty}(M)$-bimodule structure is  $\T^{n}$-equivariant. More precisely, there exist a covering
  map $c: \tilde \T^{n}\rightarrow \T^{n}$ satisfies:
  \begin{align}
    \label{eq:-equivariant-bundle}
    \tilde\alpha_{\tilde t}(f \psi) = \alpha_{c(\tilde t)}(f)\tilde
    \alpha_{\tilde t}(\psi), \,\,\,\,\,\tilde t \in \tilde \T^{n}, \psi \in \Gamma(E), f \in C^{\infty}(M).
  \end{align}
\end{defn}

\begin{defn}
  For any $s\in\R$, denoted by $\mathcal H_{s}(M)$ or $\mathcal
  H_{s}(M,E)$ to be the Hilbert space
of the $s$-th Sobolev space which is the norm completion of $C^{\infty}(M)$
or $\Gamma(E)$ with respect to the $s$-th Sobolev norm
$\norm{\cdot}_{s}$.\par
It is easy to check that $\mathcal H_{s}(M)$ and $\mathcal
  H_{s}(M,E)$ are $\T^{n}$-equivariant $C^{\infty}(M)$-bimodules. 
\end{defn}

\begin{prop}
  Let $M$ be a compact Riemannian manifold with a $\T^{n}$ action as
  isometries and $E\rightarrow M$ is a $\T^{n}$-equivariant bundle
  defined above. For each $s\in\R$, denote by $\mathcal H_{s}(M)$ and
  $\mathcal H_{s}(M,E)$ the $s$-th Sobolev space of smooth functions
  on $M$, or smooth sections of $E$, then $\mathcal H_{s}(M)$ and
  $\mathcal H_{s}(M,E)$ are $\T^{n}$-equivariant bimodules over
  $C^{\infty}(M_{\Theta})$, where the left and the right module
  structures are given by the left and the right $\times_{\Theta}$
  multiplication.  
\end{prop}



\begin{eg}[Non-commutative Two Torus] \label{eg:non-comm-two-torus}
Let $M = \T^{2} = \R^{2}/\Z^{2}$ with the induce flat metric. The
$\T^{2}$-action comes from  left translation $\alpha_{t}: \T^{2}
\rightarrow \T^{2}: \alpha_{[t]}([s]) = [t+s]$, for $s,t\in \R^{2}$,
where $[\cdot]$ means taking equivalent  class in the quotient
$\R^{2}/\Z^{2}$. \par
Let $(x_{1},x_{2})$ be the coordinates on $\T^{2}$, and $e_{1}(x_{1},x_{2}) =
e^{2\pi i x_{1}}$, $e_{2}(x_{1},x_{2}) =e^{2\pi i x_{2}}$. By
elementary Fourier theory on $\T^{2}$,
$\set{e_{1}^{k}e_{1}^{k}}_{k,l\in\Z}$ serves as basis for the
$C^{\infty}(\T^{2})$, that is 
\begin{align}
  \label{eq:Fourier-Series}
f =\nsum{(k,l)\in\Z}{} f_{(k,l)}e_{1}^{k}e_{2}^{l}, \,\,\, f\in C^{\infty}(\T^{2}),
\end{align}
moreover the Fourier coefficients $\set{f_{(k,l)}}$ are of rapidly
decay in $(k,l)$. 
  Given $t = (t_{1},t_{2})\in \R^{2}$, the torus action
is given by
\begin{align}
 \label{eq:-torus-action-nctori}
\alpha_{t}(e_{1}^{k}e_{2}^{l}) = e^{2\pi i t_{1}k+t_{2}l} e_{1}^{k}e_{2}^{l} 
\end{align}

Let $\theta \in \R\setminus\Q$, denote $\Theta = \brac{
  \begin{matrix}
    0&\theta\\ -\theta &0
  \end{matrix}
}$. The deformed algebra $C^{\infty}(\T^{2}_{\theta})$ is identical to
$C^{\infty}(\T^{2})$ as a topological vector space with the deformed
product
\begin{align}
\label{eq:deformed-product-nctorus}
  f \times_{\theta} g = \nsum{r,s\in\Z^{2}}{} e^{\pi i \abrac{r,\Theta
      s}} f_{r}g_{s} e_{1}^{r_{1}+s_{1}}e_{2}^{r_{2}+s_{2}} =
  \nsum{r,s\in\Z^{2}}{} e^{-\pi i \theta(r_{1}s_{2} - r_{2}s_{1})}
f_{r}g_{s} e_{1}^{r_{1}+s_{1}}e_{2}^{r_{2}+s_{2}},
\end{align}
where $r = (r_{1},r_{2})$, $s = (s_{1},s_{2})$, $f_{r}$ and $g_{s}$
are the Fourier coefficients of $f$ and $g$. Take $f = e_{1}$, $g =
e_{2}$ we see that 
\begin{align*}
  e_{1}\times_{\theta} e_{2} = e^{-2\pi i \theta}e_{2}\times_{\theta} e_{1}.
\end{align*}
Also the $*$-operator on $C^{\infty}(\T^{2}_{\theta})$ is the usual one by taking conjugate of a
function in $\T^{2}$. Let $\int$ be the integration against the volume
form (with respect to the flat metric) on $\T^{2}$ which is still a
trace on $C^{\infty}(\T^{2}_{\theta})$ by proposition
\ref{prop:general-set-up-prop-trace}, we define a pairing
$\phi:C^{\infty}(\T^{2}_{\theta}) \times
C^{\infty}(\T^{2}_{\theta})\rightarrow \C$: 
\begin{align*}
  \phi(f,g) = \int (g^{*}\times_{\Theta}f).
\end{align*}
Observe that the completion of the pre-Hilbert space
$(C^{\infty}(\T^{2}_{\theta}), \phi)$ is exactly the space of
$L^{2}$-functions on $\T^{2}$, we denote it by $\mathcal H_{\mathrm
  d}$. The left multiplication $f\mapsto L^{\theta}_{f}$ makes
$C^{\infty}(\T^{2}_{\theta})$ a subalgebra of $B(\mathcal H_{\mathrm
  d})$. Denote by $C(\T^{2}_{\theta})$ the operator norm completion
of  $C^{\infty}(\T^{2}_{\theta})$ which serves as the analogy of
continuous functions on the non-commutative spaces $\T^{2}_{\theta}$. \par
Recall that the non-commutative torus $\mathcal A_{\theta}$ with
$\theta\in \R\setminus \Q$ is the universal $C^{*}$-algebra generated
by two unitary elements $u$ and $v$ subjecting to the relation $uv =
e^{-2\pi i \theta} vu$. The smooth non-commutative torus $\mathcal
A^{\infty}_{\theta}$ is the subalgebra of $\mathcal A_{\theta}$
consists of formal sums $\nsum{(k,l)\in\Z^{2}}{}a_{(k,l)}u^{k}v^{l}$
with complex coefficients and the sequence $\set{a_{(k,l)}}$ is of
rapidly decay in the index $(k,l)$. There exists a canonical trace
$\varphi:\mathcal A^{\infty}_{\theta} \rightarrow \C$ defined by
\begin{align*}
  \varphi\brac{\nsum{(k,l)\in\Z^{2}}{}a_{(k,l)}u^{k}v^{l}} \defeq a_{(0,0)}.
\end{align*}
The completion of the pre-Hilbert space $(\mathcal
A_{\theta}^{\infty},\varphi)$ is denoted by $\mathcal H_{\mathrm c}$.   \par
We define a algebra homomorphism $ W: \mathcal A_{\theta}^{\infty} \rightarrow
C^{\infty}(\T_{\theta})$ by sending the generator $u$ and $v$ to
$e_{1}$ and $e_{2}$ respectively, that is  
\begin{align}\label{eq:W-iso}
 W: \mathcal A_{\theta}^{\infty} &\rightarrow  C^{\infty}(\T_{\theta})\\
          a = \nsum{r\in\Z^{2}}{}  a_{r} V^{r}  & \mapsto
          W(a) = \nsum{r\in\Z^{2}}{}  a_{r}  E^{r} \nonumber  
  \end{align}

where $E^{r} = e_{1}^{r_{1}}\times_{\theta}e^{r_{2}}_{2} \in
C^{\infty}(\T_{\theta})$ and $V^{r} = u^{r_{1}}v^{r_{2}} \in \mathcal
A_{\theta}^{\infty}$, with $r = (r_{1},r_{2}) \in \Z^{2}$.
\begin{prop}
 Keep the notations above, the map $W: \mathcal A_{\theta}^{\infty} \rightarrow
C^{\infty}(\T_{\theta})$ extends to a $C^{*}$-algebra isomorphism $W: \mathcal A_{\theta} \rightarrow
C(\T_{\theta})$. 
\end{prop}

\begin{proof}
  First, $W$ is surjective because the image of $W$
contains  $C^{\infty}(\T^{2}_{\theta})$ which is a dense subalgebra in
$C(\T^{2}_{\theta})$. For the injectivity, we need the fact that
$\mathcal A_{\theta}$ is a simple algebra for $\theta \in \R\setminus
\Q$, thus the kerel of $W$ is trivial.
\end{proof}


Since $\set{E^{r}}_{r\in\Z^{2}}$ and $\set{V^{r}}_{r\in\Z^{2}}$ are
orthonormal basises of $\mathcal H_{\mathrm d}$ and $\mathcal
H_{\mathrm c}$ respectively, then $W$ extends to a unitary map between
$\mathcal H_{\mathrm d}$ and $\mathcal H_{\mathrm c}$, denoted by
$\widetilde W$.  \par

The two torus $\R^{2}/\Z^{2}$ action on $\mathcal A_{\theta}$ is given by
\begin{align*}
  \alpha_{t}(V^{r}) \defeq e^{2\pi i t\cdot r} V^{r}, \,\,\,
  t\in\R^{2}, r\in\Z^{2}.
\end{align*}
The basic derivations representing infinitesimal generators to the
above group automorphisms are given by the defining relations:
\begin{align}
  \label{eq:nc-two-torus-derivations}
  \begin{split}
    \delta_{1}(u) &= u, \,\,\,\delta_{1}(v) = 0 \\
\delta_{2}(v) & = v,\,\,\,\delta_{2}(u) = 0;
  \end{split}
 \end{align}

The unbounded operators $\delta_{1}$ and $2\pi i \partial_{x_{1}}$,
$\delta_{2}$ and $2\pi i \partial_{x_{2}}$ are unitary equivalent with
respect to the unitary map $\widetilde W$.












\end{eg}

\begin{eg}[Non-commutative $n$-Torus]
  
\end{eg}

\begin{eg}[Non-commutative Four Sphere]
  
\end{eg}
















%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "main"
%%% End: 
