;;Color theme
;;(require 'color-theme)
;;   (setq color-theme-is-global t)
;;   (color-theme-initialize)
;;   (color-theme-arjen)
;;     (eval-after-load "color-theme" '(color-theme-arjen))
 (if (fboundp 'normal-top-level-add-subdirs-to-load-path)
         (let* ((my-lisp-dir "~/elisp/")
               (default-directory my-lisp-dir))
            (setq load-path (cons my-lisp-dir load-path))
            (normal-top-level-add-subdirs-to-load-path)))



(put 'set-goal-column 'disabled nil)
(put 'narrow-to-region 'disabled nil)
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)
(put 'LaTeX-hide-environment 'disabled nil)

(global-font-lock-mode t)

(setq dired-recursive-copies 'top)
(setq dired-recursive-deletes 'top)

;(require 'light)
;(require 'ultex-setup)

(add-hook 'LaTeX-mode-hook 'flyspell-mode)


;AucTEx
;The following is a fancier version of the hook. It sets XeLaTeX the
;default compiler when it finds fontspec or mathspec package being loaded
;with \usepackage{}.
(add-hook 'LaTeX-mode-hook #'my-latex-mode-hook)

    (defun my-latex-mode-hook ()
      (add-to-list 'TeX-command-list
                   '("XeLaTeX" "%`xelatex%(mode)%' %t" TeX-run-TeX nil t)))

    (defun my-latex-mode-hook ()
      (add-to-list 'TeX-command-list
                   '("XeLaTeX" "%`xelatex%(mode)%' %t" TeX-run-TeX nil t))
      (setq TeX-command-default
            (save-excursion
              (save-restriction
                (widen)
                (goto-char (point-min))
                (let ((re (concat "^\\s-*\\\\usepackage\\(?:\\[.*\\]\\)?"
                                  "{.*\\<\\(?:font\\|math\\)spec\\>.*}")))
                  (if (re-search-forward re 3000 t)
                      "XeLaTeX"
                    "LaTeX"))))))

(load "auctex.el" nil t t)
(load "preview-latex.el" nil t t)
(setq TeX-auto-save t)
(setq TeX-parse-self t)
(setq-default TeX-master nil)
(setq
      TeX-parse-self         t
      TeX-PDF-mode           t
      TeX-electric-sub-and-superscript 1
      preview-scale-function 1.33)
(put 'dired-find-alternate-file 'disabled nil)
(define-skeleton TeX-dollar-sign-skeleton
  "Make a pair of dollar signs and leave point inside" nil
  "$"_"$")
(global-set-key (kbd "C-c 4") 'TeX-dollar-sign-skeleton)
(setq TeX-output-view-style (quote (("^pdf$" "." "open -a PDFView.app %o") ("^dvi$" "^xdvi$" "open-x11 %(o?)xdvi %dS %d") ("^dvi$" "^TeXniscope$" "open -a TeXniscope.app %o") ("^pdf$" "." "open %o") ("^html?$" "." "open %o"))))


;org-mode settings
(require 'org-install)
(add-to-list 'auto-mode-alist '("\\.org$" . org-mode))
(define-key global-map "\C-cl" 'org-store-link)
(define-key global-map "\C-ca" 'org-agenda)
(setq org-log-done t)



;MetaPost
;(load "pi-meta")
;;
(require 'tramp)
(setq tramp-default-method "ssh")
; Define the load-path
;(pushnew (expand-file-name emacs-directory) load-path)

;;;;G-client
;(load-library "g")


;;; MacOS X specific stuff

;;ORG Mode for Dropbox
;; Set to the location of your Org files on your local system
(setq org-directory "~/Dropbox/TestOrg")
;; Set to the name of the file where new notes will be stored
(setq org-mobile-inbox-for-pull "~/Dropbox/TestOrg/inbox.org")
;;
(setq org-agenda-files(quote("~/Dropbox/TestOrg/test.org")))
;; Set to <your Dropbox root directory>/MobileOrg.
(setq org-mobile-directory "~/Dropbox/MobileOrg")

;; I disabled this since I want to avoid hitting Cmd-q accidentally.
(global-set-key [(hyper q)] 'save-buffers-kill-emacs)





;; rename-current-buffer-file
(defun rename-current-buffer-file ()
  "Renames current buffer and file it is visiting."
  (interactive)
  (let ((name (buffer-name))
        (filename (buffer-file-name)))
    (if (not (and filename (file-exists-p filename)))
        (error "Buffer '%s' is not visiting a file!" name)
      (let ((new-name (read-file-name "New name: " filename)))
        (if (get-buffer new-name)
            (error "A buffer named '%s' already exists!" new-name)
          (rename-file filename new-name 1)
          (rename-buffer new-name)
          (set-visited-file-name new-name)
          (set-buffer-modified-p nil)
          (message "File '%s' successfully renamed to '%s'"
                   name (file-name-nondirectory new-name)))))))

(global-set-key (kbd "C-x C-r") 'rename-current-buffer-file)
